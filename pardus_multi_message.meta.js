// ==UserScript==
// @name           Pardus Multi Message
// @namespace      http://userscripts.xcom-alliance.info/
// @version        3.2
// @author         Miche (Orion) / Sparkle (Artemis)
// @description    Enhances the Pardus message window to allow multiple recipients seperated by commas or newlines as well as configurable lists of pilot names that remain independent across universe. Based on work by Ivar (Artemis).
// @include        http*://*.pardus.at/sendmsg.php*
// @include        http*://*.pardus.at/msgframe.php
// @updateURL      http://userscripts.xcom-alliance.info/multi_message/pardus_multi_message.meta.js
// @downloadURL    http://userscripts.xcom-alliance.info/multi_message/pardus_multi_message.user.js
// @icon           http://userscripts.xcom-alliance.info/multi_message/icon.png
// @grant          GM_getValue
// @grant          GM_setValue
// @grant          GM_xmlhttpRequest
// @grant          GM_listValues
// @grant          GM_deleteValue
// ==/UserScript==
